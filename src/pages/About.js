import React from "react";
import Nav from "../components/Nav";
import "../styles/about.scss";

const About = () => {
	return (
		<div className="about">
			<Nav />
			<h1>About</h1>
		</div>
	);
};

export default About;
