import React, { lazy, Suspense } from "react";
import Nav from "../components/Nav";
import "../styles/Home.scss";
const DataList = lazy(() => import("../components/CardList"));

const Home = () => {
	return (
		<div className="home">
			<Nav />
			<Suspense fallback={<div className="loading">Loading...</div>}>
				<DataList />
			</Suspense>
		</div>
	);
};

export default Home;
