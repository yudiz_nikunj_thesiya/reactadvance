import React from "react";
import Nav from "../components/Nav";
import "../styles/contact.scss";

const Contact = () => {
	return (
		<div className="contact">
			<Nav />
			<h1>Contact</h1>
		</div>
	);
};

export default Contact;
