import React from "react";
import ReactDom from "react-dom";
import { IoClose } from "react-icons/io5";
import "../styles/modal.scss";
import Input from "./Input";

const Modal = ({ setOpenModal }) => {
	const mailRef = React.createRef();
	return ReactDom.createPortal(
		<div className="modal-container">
			<div className="modal">
				<div className="modal-header">
					<span className="heading">Subscribe to Newsletter</span>
					<span className="icon" onClick={() => setOpenModal(false)}>
						<IoClose />
					</span>
				</div>
				{/* <form className="modal-main">
					<div className="email">
						<IoMail />
						<input type="email" placeholder="Enter Your Email" required />
					</div>
					<button type="submit" className="btn">
						Subscribe
					</button>
				</form> */}
				<Input ref={mailRef} />
			</div>
		</div>,
		document.getElementById("portal")
	);
};

export default Modal;
