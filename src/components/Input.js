import React, { forwardRef, useEffect } from "react";
import { IoMail } from "react-icons/io5";
import toast, { Toaster } from "react-hot-toast";

const Input = forwardRef((props, ref) => {
	useEffect(() => {
		ref.current.focus();
	});
	const onsubmit = (e) => {
		e.preventDefault();
		if (ref.current.value.trim() === "") {
			toast.error("Please Fill Email Properly!");
		} else {
			toast.success(`${ref.current?.value} Subscribed to the Newsletter.`);
		}
		e.target.value = "";
	};

	return (
		<form className="modal-main">
			<div className="email">
				<IoMail />
				<input type="email" ref={ref} placeholder="Enter Your Email" required />
			</div>
			<button type="submit" className="btn" onClick={(e) => onsubmit(e)}>
				Subscribe
			</button>
			<Toaster position="bottom-right" />
		</form>
	);
});

export default Input;
