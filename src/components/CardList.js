import React from "react";
import "../styles/Home.scss";
import Card from "./Card";
import FetchData from "./FetchData";

const API = "https://api.instantwebtools.net/v1/passenger?page=6&size=10";

const CardList = ({ data }) => {
	return (
		<div className="card-container">
			{data?.map((post) => (
				<Card
					key={post._id}
					name={post?.name}
					trips={post?.trips}
					airline={post?.airline}
				/>
			))}
		</div>
	);
};

const DataList = FetchData(CardList, API);
export default DataList;
