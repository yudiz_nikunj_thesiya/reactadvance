import React, { useEffect, useState } from "react";
import "../styles/nav.scss";
import { RiMenu3Line } from "react-icons/ri";
import "../styles/verticalnav.scss";
import { MdOutlineClose } from "react-icons/md";
import { Link, NavLink } from "react-router-dom";
import Modal from "./Modal";

const Nav = () => {
	const [mobileNav, setMobileNav] = useState(false);
	const [openModal, setOpenModal] = useState(false);

	return (
		<div className="container">
			<div className="nav">
				<NavLink
					to="/"
					className={({ isActive }) => (isActive ? "active" : "inactive")}
				>
					<span className="logo">ReactAirLine</span>
				</NavLink>
				<div className="nav__menu">
					<NavLink
						to="/"
						className={({ isActive }) => (isActive ? "active" : "inactive")}
					>
						Home
					</NavLink>
					<NavLink
						to="/about"
						className={({ isActive }) => (isActive ? "active" : "inactive")}
					>
						About
					</NavLink>
					<NavLink
						to="/contact"
						className={({ isActive }) => (isActive ? "active" : "inactive")}
					>
						Contact
					</NavLink>
				</div>
				<div className="nav__btns">
					<button
						className="nav__btns--btn-2"
						onClick={() => setOpenModal(true)}
					>
						Subscribe
					</button>
					{openModal === true && <Modal setOpenModal={setOpenModal} />}
					<button
						className="nav__btns--btn-1"
						onClick={() => setMobileNav(true)}
					>
						<RiMenu3Line />
					</button>
				</div>
			</div>
			{mobileNav && (
				<div className="verticalnav">
					<div className="verticalnav__header">
						<span className="verticalnav__header--logo">ReactAirLine</span>
						<button
							className="verticalnav__header--btn"
							onClick={() => setMobileNav(false)}
						>
							<MdOutlineClose />
						</button>
					</div>
					<div className="verticalnav__menu">
						<span>
							<NavLink
								to="/"
								className={({ isActive }) => (isActive ? "active" : "inactive")}
								onClick={() => setMobileNav(false)}
							>
								Home
							</NavLink>
						</span>
						<span>
							<NavLink
								to="/about"
								className={({ isActive }) => (isActive ? "active" : "inactive")}
								onClick={() => setMobileNav(false)}
							>
								About
							</NavLink>
						</span>
						<span>
							<NavLink
								to="/contact"
								className={({ isActive }) => (isActive ? "active" : "inactive")}
								onClick={() => setMobileNav(false)}
							>
								Contact
							</NavLink>
						</span>
					</div>
				</div>
			)}
		</div>
	);
};

export default Nav;
