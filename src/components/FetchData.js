import axios from "axios";
import React, { Component } from "react";

const FetchData = (WrappedComponent, entity) => {
	return class extends Component {
		state = {
			data: [],
		};
		componentDidMount() {
			const loadPosts = async () => {
				const result = await axios.get(`${entity}`);
				this.setState({ data: result?.data });
			};
			loadPosts();
		}
		render() {
			let { data } = this.state;
			return <WrappedComponent data={data?.data}></WrappedComponent>;
		}
	};
};

export default FetchData;
