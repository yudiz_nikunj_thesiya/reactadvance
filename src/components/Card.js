import React from "react";
import "../styles/card.scss";
import PropTypes from "prop-types";

const Card = ({ name, trips, airline }) => {
	return (
		<div className="card">
			<div className="card-airline">
				<img src={airline[0]?.logo} alt="logo" />
				<span>{airline[0]?.name}</span>
				<span>{airline[0]?.country}</span>
			</div>
			<div className="card-customer">
				<span>{name}</span>
				<span>{trips}</span>
			</div>
		</div>
	);
};

export default Card;

Card.propTypes = {
	name: PropTypes.string,
	trips: PropTypes.number,
	airline: PropTypes.array,
};
